== Simple Compound Types

Arrays and tuples can be used bracket syntax or generic syntax:

[source,typescript]
----
const xs: number[] = [1, 2, 3];
const ys: Array<number> = [4, 5, 6];
----

=== Union Types

Union types use a pipe `|` to denote “OR” (a sum type, not a product type).

[source,typescript]
----
// <1>
var z = string | number = 'zero';

// <2>
z = 0;

// NOK <3>
// z = false
----

1. We say that `z` can be either a string OR a number. But it cannot be anything else, like <3>.

2. OK, it can be a number.

3. Nope, it can't be a boolean. It cannot be any other type that is not a string or a number.

=== Intersection Types

Intersection types represent values that simultaneously have multiple types.
The compound type `A & B` is a value that is both of type `A` *and* type `B`.

Because this is an intersection type, it has to fully satisfy the one or the other.
You can't mix and match.

[source,typescript,lineos]
----
include::{src_path}/e06.ts[]
----

1. Okay to call `f` with two strings.

2. Okay to call `f` with two numbers.

3. NOT okay to call `f` with any other combination of types.
It has to fully satisfy the one of the other of the compound intersection (product) type.
