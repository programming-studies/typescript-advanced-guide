= Busy TypeScript Developer’s Guide to Advanced TypeScript | Ted Neward
:toc: left
:source-highlighter: pygments
:pygments-css: class
:icons: font

- link:https://youtu.be/wD5WGkOEJRs[Video on Youtube]

This directory contains my notes on Ted Navard's brilliant talk!

//
// It is best to use empty lines between these includes.
//
// https://docs.asciidoctor.org/asciidoc/latest/directives/include/#include-syntax
//
include::./docs/01-strict-type-checking.adoc[]

include::./docs/02-structural-vs-nominal-typing.adoc[]

include::./docs/03-type-compatibility.adoc[]

include::./docs/04-simple-compound-types.adoc[]

